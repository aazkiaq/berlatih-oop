<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("shaun");
    echo "name : $sheep->name<br>"; // "shaun"
    echo "legs : $sheep->legs<br>"; // 4
    echo "cold blooded? $sheep->cold_blooded<br><br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "name : $sungokong->name<br>"; // "shaun"
    echo "legs : $sungokong->legs<br>"; // 4
    echo "cold blooded? $sungokong->cold_blooded<br>";
    echo "yell? ";
    $sungokong->yell();
    echo "<br><br>"; // "Auooo"

    $kodok = new Frog("buduk");
    echo "name : $sheep->name<br>"; // "shaun"
    echo "legs : $sheep->legs<br>"; // 4
    echo "cold blooded? $sheep->cold_blooded<br>";
    echo "jump? ";
    $kodok->jump() ; // "hop hop"
    echo "<br><br>";
?>